package com.nix.labs.lab_2;

/**
 * Created by Ju1ce on 4/7/2016.
 */

/*Напишите примеры использования данных операторов и выведите результаты на экран
        b. Сравнения
*/

public class ComparisonOperatorsExample {
    public static void main(String[] args) {

        //is a == b?
        System.out.println("is a == b?");
        System.out.println(isEqual(4, 5));
        System.out.println(isEqual(5, 4));
        System.out.println(isEqual(4, 4));

        //is a != b?
        System.out.println();
        System.out.println("is a != b?");
        System.out.println(isNotEqual(4, 5));
        System.out.println(isNotEqual(5, 4));
        System.out.println(isNotEqual(4, 4));

        //is a > b?
        System.out.println();
        System.out.println("is a > b?");
        System.out.println(isGreater(4, 5));
        System.out.println(isGreater(5, 4));
        System.out.println(isGreater(4, 4));

        //is a < b?
        System.out.println();
        System.out.println("is a < b?");
        System.out.println(isLess(4, 5));
        System.out.println(isLess(5, 4));
        System.out.println(isLess(4, 4));

        //is a >= b?
        System.out.println();
        System.out.println("is a >= b?");
        System.out.println(isGreaterOrEqual(4, 5));
        System.out.println(isGreaterOrEqual(5, 4));
        System.out.println(isGreaterOrEqual(4, 4));

        //is a <= b?
        System.out.println();
        System.out.println("is a <= b?");
        System.out.println(isLessOrEqual(4, 5));
        System.out.println(isLessOrEqual(5, 4));
        System.out.println(isLessOrEqual(4, 4));


    }

    static boolean isEqual(double a, double b) {
        return a == b;
    }

    static boolean isNotEqual(double a, double b) {
        return a != b;
    }

    static boolean isGreater(double a, double b) {
        return a > b;
    }

    static boolean isLess(double a, double b) {
        return a < b;
    }

    static boolean isGreaterOrEqual(double a, double b) {
        return a >= b;
    }

    static boolean isLessOrEqual(double a, double b) {
        return a <= b;
    }
}
