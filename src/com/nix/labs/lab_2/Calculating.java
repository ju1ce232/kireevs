package com.nix.labs.lab_2;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ju1ce on 4/8/2016.
 */

/*
4) Написать программу которая бы запросила пользователя ввести число,
        затем знак математической операции, затем еще число и вывела результат.
        Должно работать и для целых и для дробных чисел
        */

public class Calculating {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String firstNum;
        String secondNum;
        String symbol;

        System.out.println("Enter the first number: ");
        firstNum = enterNum();

        System.out.println("Enter a mathematical operation symbol (+, -, *, /, %): ");
        while (true) {
            symbol = reader.readLine();
            if ("+".equals(symbol) || "-".equals(symbol) || "*".equals(symbol) || "/".equals(symbol) || "%".equals(symbol)) {
                break;
            }
            System.out.println("Wrong operation! Please, try again using +, -. *, /, %: ");
        }

        System.out.println("Enter the second number: ");
        secondNum = enterNum();

        try {
            calculate(Float.valueOf(firstNum), Float.valueOf(secondNum), symbol);
        } catch (ArithmeticException e) {
            System.out.println("Division by zero!");
        }
    }

    //enters a value until it is a number
    private static String enterNum() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s;
        while (true) {
            s = reader.readLine();
            if (isNumber(s)) {
                break;
            }
            System.out.println("Not a number! Please, try again: ");
        }
        return s;
    }

    //checking if entered value is a number (int or double) or a string
    private static boolean isNumber(String a) {
        return a.matches("\\d") || a.matches("\\d+\\.\\d+");
    }

    //checking if entered value is int or double
    static boolean isDouble(double a) {
        return (int) (((a + 0.001) * 100) % 100) > 0;
    }

    private static void calculate(float a, float b, String s) {
        if ("+".equals(s)) {
            float sum = a + b;
            System.out.println("___________");
            if (isDouble(sum)) {
                System.out.println(a + " + " + b + " = " + sum);
            } else {
                System.out.println((int) a + " + " + (int) b + " = " + (int) sum);
            }
        }
        if ("-".equals(s)) {
            float sub = a - b;
            System.out.println("___________");
            if (isDouble(sub)) {
                System.out.println(a + " - " + b + " = " + sub);
            } else {
                System.out.println((int) a + " - " + (int) b + " = " + (int) sub);
            }
        }
        if ("*".equals(s)) {
            float mult = a * b;
            System.out.println("___________");
            if (isDouble(mult)) {
                System.out.println(a + " * " + b + " = " + mult);
            } else {
                System.out.println((int) a + " * " + (int) b + " = " + (int) mult);
            }
        }
        if ("/".equals(s)) {
            float div = a / b;
            if (0 == b) {
                throw new ArithmeticException();
            }
            System.out.println("___________");
            if (isDouble(div)) {
                System.out.println(a + " / " + b + " = " + div);
            } else {
                System.out.println((int) a + " / " + (int) b + " = " + (int) div);
            }
        }
        if ("%".equals(s)) {
            float mod = a % b;
            System.out.println("___________");
            if (isDouble(mod)) {
                System.out.println(a + " % " + b + " = " + mod);
            } else {
                System.out.println((int) a + " % " + (int) b + " = " + (int) mod);
            }
        }
    }
}
