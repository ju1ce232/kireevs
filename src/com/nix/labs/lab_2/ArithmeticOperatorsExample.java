package com.nix.labs.lab_2;

import java.io.IOException;

/**
 * Created by Ju1ce on 4/7/2016.
 */

/*Напишите примеры использования данных операторов и выведите результаты на экран
        a. Арифметические
*/

public class ArithmeticOperatorsExample {
    public static void main(String[] args) {

        //sum
        System.out.println("sum");
        System.out.println(sum(4, 5));
        System.out.println(sum(4.0, 5.1));
        System.out.println(sum(4, 5.1));
        System.out.println(sum(4.0, 5));
        //subtraction
        System.out.println();
        System.out.println("sub");
        System.out.println(sub(10, 5));
        System.out.println(sub(10.0, 5.1));
        System.out.println(sub(10, 5.1));
        System.out.println(sub(10.0, 5));
        //division
        System.out.println();
        System.out.println("div");
        System.out.println(div(10, 5));
        System.out.println(div(10.0, 5.1));
        System.out.println(div(10, 5.1));
        System.out.println(div(10.0, 5));
        //multiplication
        System.out.println();
        System.out.println("mult");
        System.out.println(mult(10, 5));
        System.out.println(mult(10.0, 5.1));
        System.out.println(mult(10, 5.1));
        System.out.println(mult(10.0, 5));
        //modulo
        System.out.println();
        System.out.println("mod");
        System.out.println(mod(26, 5));
        System.out.println(mod(10.0, 5.1));
        System.out.println(mod(10, 5.1));
        System.out.println(mod(26.0, 5));
        //increment, decrement
        int a = 5;
        int b = 5;
        System.out.println();
        System.out.println("initial a = " + a);
        System.out.println("initial b = " + b);
        a++;
        System.out.println("incremented a = " + a);
        b--;
        System.out.println("decremented b = " + b);

    }

    static int sum(int a, int b) {
        return a + b;
    }

    static double sum(double a, double b) {
        return a + b;
    }

    static int sub(int a, int b) {
        return a - b;
    }

    static double sub(double a, double b) {
        return a - b;
    }

    static double div(double a, double b) {
        return a / b;
    }

    static double mult(double a, double b) {
        return a * b;
    }

    static int mod(int a, int b) {
        return a % b;
    }

    static double mod(double a, double b) {
        return a % b;
    }

}
