package com.nix.labs.lab_2;

/**
 * Created by Ju1ce on 4/7/2016.
 */

/*
2) Переведите значение каждого примитивного типа в строку и обратно - выведите результаты на  экран
*/

public class ToStringAndBack {
    public static void main(String[] args) {
        int i = 5;
        double d = 5.5;
        boolean b = true;
        char c = 'a';
        String sI, sD, sB, sC;

        //to String
        System.out.println("Printing Strings");
        sI = Integer.toString(i);
        System.out.println(sI);

        sD = Double.toString(d);
        System.out.println(sD);

        sB = Boolean.toString(b);
        System.out.println(sB);

        sC = Character.toString(c);
        System.out.println(sC);


        //back to primitives
        System.out.println();
        System.out.println("Printing Primitives");
        i = Integer.parseInt(sI);
        System.out.println(i);

        d = Double.parseDouble(sD);
        System.out.println(d);

        b = Boolean.parseBoolean(sB);
        System.out.println(b);

        c = sC.charAt(0);
        System.out.println(c);

    }
}
