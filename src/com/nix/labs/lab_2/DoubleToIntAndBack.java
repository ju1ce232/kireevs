package com.nix.labs.lab_2;

/**
 * Created by Ju1ce on 4/7/2016.
 */

/*
3) Переведите число с точкой в тип без точки и наоборот - выведите результаты
*/

public class DoubleToIntAndBack {
    public static void main(String[] args) {
        int i = 5;
        double d = 5.5;

        System.out.println("to int");
        System.out.println("double d = " + d + " -> " + "int d (using (int)d)) = " + (int) d); //to int
        System.out.println("double d = " + d + " -> " + "int d (using new Double(d).intValue()) = " + new Double(d).intValue()); //to int

        System.out.println();
        System.out.println("to double");
        System.out.println("int i = " + i + " -> " + "double i (using (double)i) = " + (double) i); //to double
        System.out.println("int i = " + i + " -> " + "double i (using 1.0 * 5) = " + 1.0 * 5); //to double
        System.out.print("int i = " + i + " -> " + "double d (using d = i) = ");
        d = i; //int i to double d
        System.out.print(d);

    }
}
