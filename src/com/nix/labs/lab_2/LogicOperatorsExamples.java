package com.nix.labs.lab_2;

import com.sun.org.apache.xpath.internal.SourceTree;

/**
 * Created by Ju1ce on 4/7/2016.
 */

/*Напишите примеры использования данных операторов и выведите результаты на экран
        с. Логические
        d. Приравнивания*/

public class LogicOperatorsExamples {
    public static void main(String[] args) {
        int a = 5;
        int b = 10;
        int c = a;
        int d = 15;
        boolean e = false;

        if ((a + b) > d) {
            System.out.println("1"); //won't be printed
        } else if (!e && (b < d)) {
            System.out.println("Testing !e == true && (b < d)");
            System.out.println("!" + e + " == true && (" + b + " < " + d + ") ==> true");
        }

        System.out.println();

        if ((b > a) || (d >= b)) {
            System.out.println("Testing (b > a) || (d >= b)");
            System.out.println("(" + b + " > " + a + ") || (" + d + " >= " + b + ") ==> true");
        }

        System.out.println();

        if ((a == d - b) | (d < c)) {
            System.out.println("Testing (a == d - b) | (d < c)");
            System.out.println("Even if \"(a == d - b) ==> true\", \"d < c\" will be checked anyway");
        }


    }
}
