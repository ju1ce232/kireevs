package com.nix.labs.lab_3;

/**
 * Created by Ju1ce on 4/8/2016.
 */

/*
При помощи цикла for вывести на экран нечетные числа от 1 до 99.
* */

public class Task_2 {
    public static void main(String[] args) {
        for (int i = 1; i < 100; i=i+2) {
            System.out.println(i);
        }
    }
}
