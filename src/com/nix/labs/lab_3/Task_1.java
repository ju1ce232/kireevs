package com.nix.labs.lab_3;

import java.util.Arrays;

/**
 * Created by Ju1ce on 4/8/2016.
 */

/*
Дан массив из 4 чисел типа int. Сравнить их и вывести наименьшее на консоль.
*/

public class Task_1 {
    public static void main(String[] args) {
        int[] list = generator(4);
        showMin(list);
    }

    private static void showMin(int[] list) {
        int min = list[0];
        for (int i = 1; i < list.length; i++) {
            if (list[i] < min) {
                min = list[i];
            }
        }

//        Arrays.sort(list);
//        min = list[0];

        System.out.println();
        System.out.println("The minimal number is: " + min);
    }

    //initializing random array
    private static int[] generator(int size) {
        int[] list = new int[size];
        System.out.print("Generated array: ");
        for (int i = 0; i < list.length; i++) {
            int n = (int) (Math.random() * 9 + 1);
            list[i] = n;
            System.out.print(list[i] + " ");
        }
        return list;
    }


}
