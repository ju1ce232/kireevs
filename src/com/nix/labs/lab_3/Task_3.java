package com.nix.labs.lab_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * Created by Ju1ce on 4/8/2016.
 */

/*
Вывести все простые числа от 1 до N. N задается пользователем через консоль
* */

public class Task_3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите целое число: ");
        long num = 0;
        try {
            num = Long.parseLong(reader.readLine());
        } catch (NumberFormatException e) {
            System.out.println("Oops, incorrect input...");
        }

        showSimple(num);
    }

    private static void showSimple(long num) {

        if (num < 2) {
            System.out.println("Нет простых чисел");
        }
        else {
            System.out.println(2);
            OUTER:
            for (int i = 1; i <= num; i += 2) {
                for (int j = 2; j < (i / 2 + 1); j++) {
                    if (i % j == 0) {
                        continue OUTER;
                    }
                }
                if (i > 1){
                System.out.println(i);
                }
            }
        }

    }
}

