package com.nix.labs.lab_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ju1ce on 4/8/2016.
 */

/*
Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
*/

public class Task_5 {
    public static void main(String[] args) throws IOException {
        float sum = 0;
        String s = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Вводите числа для подсчета суммы. Введите \"Сумма\" для вывода суммы: ");

        while (true){
            s = reader.readLine();
            if (isNumber(s)){
                sum += Double.parseDouble(s);
            }
            if ("Сумма".equals(s)) {
                break;
            }
            if (!isNumber(s)){
                System.out.println("Некорректный ввод, повторите ввод или введите \"Сумма\": ");
            }
        }

        System.out.println();
        System.out.println("Сумма: " + sum);
    }

    private static boolean isNumber(String a) {
        return a.matches("\\d") || a.matches("\\d+\\.\\d+");
    }
}
