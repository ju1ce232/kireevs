package com.nix.labs.lab_3;

/**
 * Created by Ju1ce on 4/8/2016.
 */

/*
Дан массив из 4 чисел типа int. Вывести их все.
* */

public class Task_4 {
    public static void main(String[] args) {
        //initializing random array
        int[] list = new int[4];
        System.out.println("Generated array: ");
        for (int i = 0; i < list.length; i++) {
            int n = (int) (Math.random() * 9 + 1);
            list[i] = n;
        }

        for (int i : list) {
            System.out.println(i);
        }
    }
}
