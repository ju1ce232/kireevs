package com.nix.labs.lab_5;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.logging.SimpleFormatter;

/**
 * Created by Ju1ce on 4/18/2016.
 */

/*Реализовать свою вариант List
Используя свой класс - сделать коллекцию дат на 30 елементов
Проверить день каждой даты.
Если это будний день - кинуть свое исключение наследованное от Exception
Если же это выходной - кинуть свое исключение наследованное от RuntimeException
Вывести результат в виде
Дата - будний день \ Дата - выходной
*/

public class Solution {
    public static void main(String[] args) {
        MyDatesList myDatesList = new MyDatesList();

        for (int i = 0; i < 30; i++) {
            int y = (int) (Math.random() * 9 + 2000);
            int m = (int) (Math.random() * 11 + 1);
            int d = (int) (Math.random() * 27 + 1);

            Calendar date = new GregorianCalendar(y, m, d);
            myDatesList.add(date);
        }

        //testing get
//        for (int i = 0; i < myDatesList.size(); i++) {
//            System.out.println(myDatesList.get(i));
//        }
//        System.out.println();

        //testing set, indexOf and lastIndexOf
//        Calendar test = new GregorianCalendar(2099, 11, 21);
//        myDatesList.set(15, test);
//        System.out.println(myDatesList.indexOf(test));
//        System.out.println(myDatesList.lastIndexOf(test));

        //testing subList
//        System.out.println(myDatesList.subList(2,9));

        //testing foreach
//        for (Object o : myDatesList){
//            System.out.println(o);
//        }

        //testing remove
//        myDatesList.remove(0);

        //ниже, весь закоменченный код относится к SDF - год выводится +1900
//        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy, EEEE");
//        Calendar cal = GregorianCalendar.getInstance();

        for (int i = 0; i < myDatesList.size(); i++) {
//            Date d = (Date)myDatesList.get(i);
//            cal.set(d.getYear(), d.getMonth(), d.getDay());

            Calendar d = (Calendar) myDatesList.get(i);
            int day = d.get(Calendar.DAY_OF_WEEK);
            try {
                if (1 == day || 7 == day) {
                    throw new MyRuntimeException();
                } else {
                    throw new MyException();
                }
            } catch (RuntimeException e) {
                System.out.println(d.getTime() + " - выходной день");
            } catch (Exception e) {
                System.out.println(d.getTime() + " - будний день");
            }
//              catch (RuntimeException e) {
//                System.out.println(sdf.format(d) + " - Выходной");
//            } catch (Exception e) {
//                System.out.println(sdf.format(d) + " - Будний");
//            }
        }
    }
}
