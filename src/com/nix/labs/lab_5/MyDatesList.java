package com.nix.labs.lab_5;

import java.util.*;

/**
 * Created by Ju1ce on 4/19/2016.
 */
public class MyDatesList implements List {
    private static final int DEFAULT_INITIAL_CAPACITY = 10;
    private int size;
    private Object[] myDatesList;
    private static final Object[] EMPTY_ELEMENTDATA = {};

    public MyDatesList() {
        this.myDatesList = new Object[DEFAULT_INITIAL_CAPACITY];
    }

    public MyDatesList(Object[] myDatesList) {
        this.myDatesList = myDatesList;
    }

    public MyDatesList(int initialCapacity) {
        if (initialCapacity > 0) {
            this.myDatesList = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            this.myDatesList = EMPTY_ELEMENTDATA;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " +
                    initialCapacity);
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(myDatesList[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size - 1; i >= 0; i--) {
            if (o.equals(myDatesList[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(myDatesList, size);
    }

    @Override
    public Object[] toArray(Object[] a) {
        return Arrays.copyOf(myDatesList, size);
    }

    @Override
    public boolean add(Object o) {
        if (size >= myDatesList.length) {
            int oldCapacity = myDatesList.length;
            int newCapacity = oldCapacity + (oldCapacity >> 1);
            myDatesList = Arrays.copyOf(myDatesList, newCapacity);
        }
        myDatesList[size++] = o;
        return true;
    }

    @Override
    public void add(int index, Object element) {
        if (index > size || index < 0)
            throw new IndexOutOfBoundsException();
        if (size <= index) {
            int oldCapacity = myDatesList.length;
            int newCapacity = oldCapacity + index;
            myDatesList = Arrays.copyOf(myDatesList, newCapacity);
        }
        System.arraycopy(myDatesList, index, myDatesList, index + 1, size - index);
        myDatesList[index] = element;
        size++;
    }

    @Override
    public boolean remove(Object o) {
        for (int index = 0; index < size; index++)
            if (o.equals(myDatesList[index])) {
                MyDatesList.this.remove(index);
                myDatesList[--size] = null;
                break;
            }
        return false;
    }

    @Override
    public Object remove(int index) {
        Object oldValue = myDatesList[index];
        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(myDatesList, index + 1, myDatesList, index, numMoved);
        myDatesList[--size] = null;

        return oldValue;
    }

    @Override
    public boolean addAll(Collection c) {
        Object[] a = c.toArray();
        int numNew = a.length;
        if ((size + numNew) - myDatesList.length > 0) {
            int oldCapacity = myDatesList.length;
            int newCapacity = oldCapacity + numNew + 1;
            myDatesList = Arrays.copyOf(myDatesList, newCapacity);
        }
        System.arraycopy(a, 0, myDatesList, size, numNew);
        size += numNew;
        return numNew != 0;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        Object[] a = c.toArray();
        int numNew = a.length;
        if ((size + numNew) - myDatesList.length > 0) {
            int oldCapacity = myDatesList.length;
            int newCapacity = oldCapacity + numNew + 1;
            myDatesList = Arrays.copyOf(myDatesList, newCapacity);
        }
        int numMoved = size - index;
        if (numMoved > 0) {
            System.arraycopy(myDatesList, index, myDatesList, index + numNew, numMoved);
        }
        System.arraycopy(a, 0, myDatesList, index, numNew);
        size += numNew;
        return numNew != 0;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        Object[] subLst = new Object[this.size - (toIndex - fromIndex)];
        int j = 0;
        for (int i = 0; i < this.size; i++) {
            if (i < fromIndex || i >= toIndex) {
                subLst[j++] = myDatesList[i];
            }
        }
        return new MyDatesList(subLst);
    }

    @Override
    public boolean retainAll(Collection c) {
        for (int i = 0; i < myDatesList.length; i++) {
            for (Object o : c) {
                if (o.equals(myDatesList[i])) {
                    MyDatesList.this.remove(i);
                    i--;
                    break;
                }
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection c) {
        for (Object o : c) {
            for (int i = 0; i < myDatesList.length; i++) {
                if (myDatesList[i].equals(o)) {
                    MyDatesList.this.remove(i);
                    i--;
                }
            }
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection c) {
        boolean count;
        for (Object o : c) {
            count = false;
            for (Object aMyDatesList : myDatesList) {
                if (aMyDatesList.equals(o)) {
                    count = true;
                    break;
                }
            }
            if (!count) return false;
        }
        return true;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++)
            myDatesList[i] = null;
        size = 0;
    }

    @Override
    public Object get(int index) {
        return myDatesList[index];
    }

    @Override
    public Object set(int index, Object element) {
        Object oldValue = myDatesList[index];
        myDatesList[index] = element;
        return oldValue;
    }

    @Override
    public Iterator iterator() {
        return new Itr();
    }

    private class Itr implements Iterator {
        int cursor = -1;

        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        @Override
        public Object next() {
            cursor++;
            return myDatesList[cursor];
        }
    }

    @Override
    public ListIterator listIterator() {
        return new ListItr(0);
    }

    @Override
    public ListIterator listIterator(int index) {
        return new ListItr(index);
    }

    private class ListItr extends Itr implements ListIterator {
        ListItr(int index) {
            super();
            cursor = index;
        }

        @Override
        public boolean hasPrevious() {
            return cursor != 0;
        }

        @Override
        public Object previous() {
            cursor = cursor - 1;
            return myDatesList[cursor];
        }

        @Override
        public int nextIndex() {
            return cursor;
        }

        @Override
        public int previousIndex() {
            return cursor - 1;
        }

        @Override
        public void remove() {
            MyDatesList.this.remove(cursor);
        }

        @Override
        public void set(Object o) {
            MyDatesList.this.set(cursor, o);
        }

        @Override
        public void add(Object o) {
            MyDatesList.this.add(o);
        }
    }

}
