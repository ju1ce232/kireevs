package com.nix.labs.lab_4;

/**
 * Created by juice on 13.04.2016.
 */
public class AutoType2 extends Auto {
    double baseAcceleration = getAcceleration();

    public AutoType2(double maxSpeed, double acceleration, double mobility) {
        super(maxSpeed, acceleration, mobility);
    }

    @Override
    public void turn() {
        setAcceleration(baseAcceleration);
        setSpeedStart(getSpeedEnd() * getMobility());
        if (getSpeedStart() < getMaxSpeed() / 2) {
            setAcceleration(getAcceleration() * 2);
        }
    }
}
