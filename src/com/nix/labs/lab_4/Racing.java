package com.nix.labs.lab_4;

import java.util.ArrayList;

import static com.nix.labs.lab_4.CreateAutos.createAutos;
import static com.nix.labs.lab_4.PrintAndSort.printResults;
import static com.nix.labs.lab_4.PrintAndSort.sortByLowestTime;

/**
 * Created by juice on 12.04.2016.
 */

/*Реализовать гонки
        Даны три водителя с одинаковым навыком вождения (Этим параметром можно пренебречь).
        Они используют три разных автомобиля. Трасса состоит из 20 прямых отрезков и поворотов между каждым отрезком.
        аждый автомобиль едет один на трассе. У каждого автомобиля есть 3 характеристики - Максимальная скорость, Ускорение и Маневренность.
        Скорость измеряется в км\ч
        Ускорение измеряется  в м\с
        Маневренность - коэффициент потери скорости в момент поворота от 0 до 1 (при маневренности = 0,4 - во время поворота потеряет 60% скорости (Пример 10*0,4 = 4))
        Каждый прямой отрезок - 2 км
        Во время поворота
        У каждого автомобиля есть своя особенность
        1ый - если скорость автомобиля на момент поворота больше половины от максимальной - то он получает +0,5% от разницы между половиной от максимальной
        и нынешней к маневренности на этот поворот следующим образом (половина максимальной = 75. Нынешняя равна 90. Разница = 15. Маневренность
        до срабатывания = 0,5, стала = 0,575)
        2ой - если скорость автомобиля после поворота меньше чем половина от максимальной то его ускорение на этом отрезке увеличивается в 2 раза
        3ий - если автомобиль в момент поворота достиг своей максимальной скорости, то его максимальная скорость увеличивается на 10% до конца гонки

        После реализации создать несколько наборов автомобилей с разными характеристиками и прогнать их на трассе вывести результаты на экран - в виде
        списка записей следующего типа:
        Автомобиль 1 прошел трассу за 10 минут 5 секунд
        отсортированного от меньшего результата к большему.
        Алгоритм сортировки реализовать свой - без использования сторонних библиотек.*/

public class Racing {
    public static void main(String[] args) {
        Auto[] auto = createAutos();

        for (Auto cars : auto) {
            cars.startRacing();
        }

        sortByLowestTime(auto);
        printResults(auto);
    }


}
