package com.nix.labs.lab_4;

/**
 * Created by Ju1ce on 4/15/2016.
 */
public class PrintAndSort {
    public static void sortByLowestTime(Auto[] autos) {
        for (int i = 0; i < autos.length - 1; i++) {
            for (int j = i + 1; j < autos.length; j++) {
                Auto tmp;
                if (autos[j].getTime() < autos[i].getTime()) {
                    tmp = autos[i];
                    autos[i] = autos[j];
                    autos[j] = tmp;
                }
            }
        }
    }

    public static void printResults(Auto[] autos) {
        for (int i = 0; i < autos.length; i++) {
            System.out.println("Автомобиль "
                    + (i + 1) + " прошел трассу за "
                    + (int) autos[i].getTime() / 60 + " мин "
                    + (int) autos[i].getTime() % 60 + " сек");
        }
    }
}
