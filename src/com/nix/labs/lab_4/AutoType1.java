package com.nix.labs.lab_4;

/**
 * Created by juice on 13.04.2016.
 */
public class AutoType1 extends Auto {
    public AutoType1(double maxSpeed, double acceleration, double mobility) {
        super(maxSpeed, acceleration, mobility);
    }

    @Override
    public void turn() {
        if (getSpeedEnd() > getMaxSpeed() / 2) {
            setMobility(getMobility() + (getSpeedEnd() - getMaxSpeed() / 2) * 0.005);
            setSpeedStart(getSpeedEnd() * getMobility());
        } else {
            setSpeedStart(getSpeedEnd() * getMobility());
        }
    }
}
