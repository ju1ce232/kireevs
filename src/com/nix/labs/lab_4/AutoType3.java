package com.nix.labs.lab_4;

/**
 * Created by juice on 13.04.2016.
 */
public class AutoType3 extends Auto {
    public AutoType3(double maxSpeed, double acceleration, double mobility) {
        super(maxSpeed, acceleration, mobility);
    }

    @Override
    public void turn() {
        if (getSpeedEnd() == getMaxSpeed()) {
            setMaxSpeed(getMaxSpeed() + getMaxSpeed() * 0.1);
        }
        setSpeedStart(getSpeedStart() * getMobility());
    }
}
