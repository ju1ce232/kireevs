package com.nix.labs.lab_4;

/**
 * Created by Ju1ce on 4/15/2016.
 */
public class CreateAutos {
    public static Auto[] createAutos(){
        Auto[] auto = new Auto[6];
        auto[0] = new AutoType1(250, 2, 0.5);
        auto[1] = new AutoType2(220, 2.5, 0.4);
        auto[2] = new AutoType3(250, 4, 0.3);
        auto[3] = new AutoType1(255, 2.1, 0.3);
        auto[4] = new AutoType2(200, 2, 0.7);
        auto[5] = new AutoType3(280, 2.8, 0.45);

        return auto;
    }
}
