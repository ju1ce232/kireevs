package com.nix.labs.lab_4;

/**
 * Created by juice on 13.04.2016.
 */
public abstract class Auto implements Turnable {
    private double maxSpeed; // макс скорость
    private double acceleration; // ускорение
    private double mobility; // маневренность
    public final static int line = 2000; // макс длина прямой
    public final static int lineNum = 20; // макс число прямых
    private double speedStart = 0; // начальная скорость на каждой прямой
    private double speedEnd = 0; // конечная скорость, достигнутая при завершении прямой
    private long time = 0; // накапливает время

    public Auto(double maxSpeed, double acceleration, double mobility) {
        this.maxSpeed = maxSpeed * 1000 / 3600; // км/ч в м/с
        this.acceleration = acceleration;
        this.mobility = mobility;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    public double getMobility() {
        return mobility;
    }

    public void setMobility(double mobility) {
        this.mobility = mobility;
    }

    public double getSpeedStart() {
        return speedStart;
    }

    public void setSpeedStart(double speedStart) {
        this.speedStart = speedStart;
    }

    public double getSpeedEnd() {
        return speedEnd;
    }

    public void setSpeedEnd(double speedEnd) {
        this.speedEnd = speedEnd;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    // гонка - пробег всех прямых и поворотов
    public void startRacing() {
        for (int i = 0; i < lineNum; i++) {
            runLine();
            if (i < lineNum - 1){
                turn();
            }
        }
    }

    // считает время на прямых
    public void runLine() {
        setSpeedEnd(Math.sqrt(2 * getAcceleration() * line + getSpeedStart() * getSpeedStart())); // скорость на конце прямой
        if (getSpeedEnd() > getMaxSpeed()) {
            setSpeedEnd(getMaxSpeed());
            double distanceToGetMaxSpeed = ((getMaxSpeed() * getMaxSpeed() - getSpeedStart() * getSpeedEnd()) / 2); // расстояние пройденное при достижении макс скорости
            long t1 = (long) ((getMaxSpeed() - getSpeedStart()) / getAcceleration()); // время на равноускоренном участке
            long t2 = (long) ((line - distanceToGetMaxSpeed) / getMaxSpeed()); // время на равномерном участке
            setTime((getTime() + t1 + t2)); // общее время прямой
        } else {
            setTime((long) (getTime() + (getSpeedEnd() - getSpeedStart()) / getAcceleration())); // только "равноускоренное" время
        }
        setSpeedStart(getSpeedEnd());
    }
}
